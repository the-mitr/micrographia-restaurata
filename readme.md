About

Robert Hooke's Micrographia was a seminal book that launched an entire discipline and at the same time allowed visions of a world unseen by human eyes before. The illustrations of everyday objects from the lens of a microscope bring to fore astonishment for the structure at the small scale and at the same time evoke a strong sense of *jamais vu* in the viewers. A few years after Micrographia was published, its plates were rediscovered and were printed again the in the form of a book which was titled **Micrographia Restaurata.** This project aims to recreate this book using modern typesetting technique of LaTeX and make it widely reachable. The major change in the design is that the images are placed next to their descriptions instead of being at the back of the book. Also images in a given plate are distributed instead of a single plate having multiple images. This helps in contextualising the images to their descriptions in the text.

Some resources used in the project

At the Biodiversity Heritage Library / Internet Archive

https://archive.org/details/micrographiarest00hook

https://archive.org/details/b30451048/page/n4/mode/2up

https://www.biodiversitylibrary.org/bibliography/107919#/summary

Cleaned Text 

https://www.chlt.org/sandbox/lhl/Hooke1745/page.h.php

Very high resolution images

[https://digital.sciencehistory.org/works/9g54xj51s/viewer/jq085m095](https://digital.sciencehistory.org/works/9g54xj51s/viewer/jq085m095)

[http://lhldigital.lindahall.org/cdm/ref/collection/nat_hist/id/0](http://lhldigital.lindahall.org/cdm/ref/collection/nat_hist/id/0)



 Welcome collection

https://wellcomecollection.org/works/r6tzzygb/items?canvas=119&langCode=eng&sierraId=b30451048



Project Gutenberg - original Micrographia

https://www.gutenberg.org/files/15491/15491-h/15491-h.htm


